#***************************************************************************
#
#   File        : makefile
#   Student Id  : 757931
#   Name        : Sebastian Baker
#
#***************************************************************************

CC=gcc
FLAGS=-Wall -lssl -lcrypto
OUT=certcheck
TESTS=sample_certs
SRC=src/main.c
EXAMPLE_SRC=./certexample.c
CSVOUT=output.csv

compile:
	$(CC) $(SRC) $(FLAGS) -o $(OUT);

clean:
	rm $(OUT); rm $(CSVOUT);

clean_tests:
	rm $(TESTS)/$(OUT); rm $(TESTS)/$(CSVOUT);

compile_tests:
	$(CC) $(SRC) $(FLAGS) -o $(TESTS)/$(OUT);

test:
	cd sample_certs/; ./testscript.sh ; cd ..;
